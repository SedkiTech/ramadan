const express = require("express");
const router = express.Router();

//import controllers
const cooktimeController = require("../controllers/cooktimeController");

router.route("/alldishes").get(cooktimeController.getAllDishes);

router.route("/getduration").get(cooktimeController.getDuration);

router.route("/getingredient").get(cooktimeController.getIngredient);




module.exports = router;
