const express = require("express");
const router = express.Router();

//import controllers
const projectController = require("../controllers/projectController");

router.route("/").get(projectController.getAllProjects);
router.route("/alldishes").get(projectController.getAllDishes);

router
  .route("/")
  .post(projectController.checkBody, projectController.createProject);

router
  .route("/getProjectsWithOutTasks")
  .get(projectController.getProjectsWithOutTasks);

router
  .route("/:status")
  .get(projectController.getProjectsWithTasks);

router.route("/:id").delete(projectController.deleteProject);

router
  .route("/getOneProjectHaveTasks")
  .get(projectController.getOneProjectHaveTasks);

//router.route("/update").get(projectController.updateProject);

module.exports = router;
