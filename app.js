const express = require("express");
const app = express();
const morgan = require("morgan");
//import routes
// const projectRouter = require("./routes/projectRoutes");
const cooktimeRouter = require("./routes/cooktimeRoutes");


// middleware
app.use(express.json());
app.use(morgan("dev"));

// mount routes
// app.use("/api/v1/projects", projectRouter);
app.use("/api/v1/cooktime", cooktimeRouter);


// start the server
const port = 3001;
app.listen(port, () => {
  console.log(`app running on port ${port}`);
});
