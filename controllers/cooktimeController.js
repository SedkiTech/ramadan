
const fetch = require("cross-fetch");
const fs = require("fs");

// get DATA
const dishes = JSON.parse(
  fs.readFileSync(`${__dirname}/../dev-data/data/dishes.json`)
);

//Get prayer times in sousse
const url = "http://api.aladhan.com/v1/calendar?latitude=35.82390132656322&longitude=10.61654904694165&method=2&month=3&year=2022";

// Get All dishes
exports.getAllDishes = (req, res) => {
  // console.log(req.body);

  res.status(201).json({
    status: "success",
    results: dishes.length,
    data: {
      Alldishes: dishes,
    },
  });
};


// Get ingredients 

exports.getIngredient = (async (req, res) => {


  const { ingredient } = req.query;
  // console.log("ingredient ==> :", ingredient)
  const newArray = dishes.map((dish) => dish.ingredients.find((ing) => ing == ingredient));

  res.status(201).json({
    status: "success",
    results: newArray.length,
    data: {
      newArray,
    },
  });
});




// (async () => {
//   try {
//     const res = await fetch(url);

//     if (res.status >= 400) {
//       throw new Error("Bad response from server");
//     }

//     const data = await res.json();
//   } catch (err) {
//     console.error(err);
//   }
// })();









// Get duration
exports.getDuration = (req, res) => {

  const { duration } = req.query;
  console.log("req.query.duration ==> :", duration)
  const newArray = dishes.filter((el) => el.duration == duration);

  res.status(201).json({
    status: "success",
    results: newArray.length,
    data: {
      getDuration: newArray,
    },
  });
};