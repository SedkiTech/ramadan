const fs = require("fs");

// get DATA
const projects = JSON.parse(
  fs.readFileSync(`${__dirname}/../dev-data/data/project.json`)
);

const dishes = JSON.parse(
  fs.readFileSync(`${__dirname}/../dev-data/data/dishes.json`)
);

// Get All Projects
exports.getAllProjects = (req, res) => {
  console.log(req.body);

  res.status(201).json({
    status: "success",
    results: projects.length,
    data: {
      AllProjects: projects,
    },
  });
};


// Get All dishes
exports.getAllDishes = (req, res) => {
  console.log(req.body);

  res.status(201).json({
    status: "success",
    results: dishes.length,
    data: {
      Alldishes: dishes,
    },
  });
};

// Get projects that have no tasks
exports.getProjectsWithOutTasks = (req, res) => {
  const newArray = projects.filter((el) => el.tasks == undefined);
  res.status(201).json({
    status: "success",
    results: newArray.length,
    data: {
      projectsWithoutTasks: newArray,
    },
  });
};

// Get projects that have tasks///////////////////////////////
exports.getProjectsWithTasks = (req, res) => {

  const { status } = req.query;
  console.log("req.query.status", status)
  const newArray = projects.filter((el) => el.status == status);

  res.status(201).json({
    status: "success",
    results: newArray.length,
    data: {
      getProjectsWithTasks: newArray,
    },
  });
};
///////////////////////////////////////////////////////
// Get ONE project Have Tasks
exports.getOneProjectHaveTasks = (req, res) => {

  const newArray = projects.filter((el) => el.tasks !== undefined);
  res.status(201).json({
    status: "success",
    results: newArray.length,
    data: {
      getProjectsWithTasks: newArray[0],
    },
  });
};

exports.checkBody = (req, res, next) => {
  if (
    !req.body.title &&
    !req.body.techno &&
    !req.body.launch - date &&
    !req.body.status
  ) {
    return res.status(404).json({
      status: "fail",
      message: "missing fields",
    });
  }
  next();
};

// POST request
exports.createProject = (req, res) => {
  const newId = projects[projects.length - 1].id + 1;

  const newProject = Object.assign({ id: newId }, req.body);

  projects.push(newProject);

  fs.writeFile(
    `${__dirname}/dev-data/data/project.json`,
    JSON.stringify(projects),
    (err) => {
      res.status(201).json({
        status: "success",
        data: {
          tour: newProject,
        },
      });
    }
  );
};

// DELETE request
exports.deleteProject = (req, res) => {
  const myid = req.params.id * 1;
  const newArray = projects.filter((el) => el.id !== myid);
  fs.writeFile(
    `${__dirname}/../dev-data/data/project.json`,
    JSON.stringify(newArray),
    (err) => {
      res.status(204).json({
        status: "success",
        message: "project deleted",
      });
    }
  );
};
